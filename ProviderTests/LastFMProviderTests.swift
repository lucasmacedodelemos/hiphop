//
//  LastFMProviderTests.swift
//  ProviderTests
//
//  Created by Lucas Macedo de Lemos on 22/07/19.
//  Copyright © 2019 Hip Hop. All rights reserved.
//

import XCTest
import Model
@testable import Provider


class LastFMProviderTests: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testHipHopTopAlbums() {
        let expetacion = self.expectation(description: "hip_hop_top_albums")
        var topAlbums: TopAlbums?

        LastFMProvider.fetchTopAlbums(of: "hip hop") { (result) in
            switch result {
            case let .success(albums):
                topAlbums = albums
            case .failure(_):
                break
            }
            
            expetacion.fulfill()
        }
        
        waitForExpectations(timeout: 8, handler: nil)
        XCTAssertNotNil(topAlbums)
    }

}
