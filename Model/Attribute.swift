//
//  Attribute.swift
//  Model
//
//  Created by Lucas Macedo de Lemos on 22/07/19.
//  Copyright © 2019 Hip Hop. All rights reserved.
//

import Foundation

public struct Attribute: Codable {
    
    public var tag: String
    public var page: String
    public var perPage: String
    public var totalPages: String
    public var total: String
}

public struct AlbumAttribute: Codable {
    
    public let rank: String
}

public struct TrackAttribute: Codable {
    
    public let rank: String
}
