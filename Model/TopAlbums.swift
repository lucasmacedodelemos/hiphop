//
//  TopAlbums.swift
//  Model
//
//  Created by Lucas Macedo de Lemos on 22/07/19.
//  Copyright © 2019 Hip Hop. All rights reserved.
//

import Foundation

public struct TopAlbums: Codable {
    
    public var albums: Albums
}
