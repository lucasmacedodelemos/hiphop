//
//  AlbumDetails.swift
//  Model
//
//  Created by Lucas Macedo de Lemos on 26/07/19.
//  Copyright © 2019 Hip Hop. All rights reserved.
//

import Foundation

public struct AlbumDetails: Codable {
    
    public var album: Details
}

public struct Details: Codable {
    public let tracks: Tracks
    public let listeners: String
    public let playcount: String
    public let wiki: Wiki
}


