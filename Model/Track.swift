//
//  Track.swift
//  Model
//
//  Created by Lucas Macedo de Lemos on 26/07/19.
//  Copyright © 2019 Hip Hop. All rights reserved.
//

import Foundation

public struct Tracks: Codable {
    public let track: [Track]
}

public struct Track: Codable {
    public let name: String
    public let url: String
    public let duration: String
    public let attribute: TrackAttribute
    public let streamable: Streamable
    public let artist: Artist
    
    enum CodingKeys: String, CodingKey {
        case attribute = "@attr"
        case name
        case url
        case artist
        case duration
        case streamable
    }
}
