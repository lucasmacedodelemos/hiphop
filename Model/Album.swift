//
//  Album.swift
//  Model
//
//  Created by Lucas Macedo de Lemos on 22/07/19.
//  Copyright © 2019 Hip Hop. All rights reserved.
//

import Foundation

public struct Albums: Codable {
    
    public var album: [Album]
    public var attribute: Attribute
    
    enum CodingKeys: String, CodingKey {
        case attribute = "@attr"
        case album
    }
}

public struct Album: Codable {
    
    public let name: String
    public let mbid: String
    public let url: String
    public let artist: Artist
    public let image: [Image]
    public let attribute: AlbumAttribute
    
    public enum ImageType: String {
        case small = "small"
        case medium = "medium"
        case large = "large"
        case extralarge = "extralarge"
    }
    
    public func imageText(of type: ImageType) -> String {
        return image.first { (image) -> Bool in
            image.size == type.rawValue
        }?.text ?? ""
    }
    
    enum CodingKeys: String, CodingKey {
        case attribute = "@attr"
        case name
        case mbid
        case url
        case artist
        case image
    }
}
