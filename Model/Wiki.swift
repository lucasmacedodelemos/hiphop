//
//  Wiki.swift
//  Model
//
//  Created by Lucas Macedo de Lemos on 26/07/19.
//  Copyright © 2019 Hip Hop. All rights reserved.
//

import Foundation

public struct Wiki: Codable {
    public let published: String
    public let summary: String
    public let content: String
}
