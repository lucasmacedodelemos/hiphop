//
//  Image.swift
//  Model
//
//  Created by Lucas Macedo de Lemos on 22/07/19.
//  Copyright © 2019 Hip Hop. All rights reserved.
//

import Foundation

public struct Image: Codable {
    
    public let text: String
    public let size: String
    
    enum CodingKeys: String, CodingKey {
        case text = "#text"
        case size
    }
}
