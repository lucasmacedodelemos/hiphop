//
//  Artist.swift
//  Model
//
//  Created by Lucas Macedo de Lemos on 22/07/19.
//  Copyright © 2019 Hip Hop. All rights reserved.
//

import Foundation

public struct Artist: Codable {
    
    public let name: String
    public let mbid: String
    public let url: String
    public let stats: Stats?
}

public struct Stats: Codable {
    
    public let listeners: String
    public let playcount: String
}


