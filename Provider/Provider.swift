//
//  Provider.swift
//  Provider
//
//  Created by Lucas Macedo de Lemos on 19/07/19.
//  Copyright © 2019 Hip Hop. All rights reserved.
//

import Foundation
import Util

public class Provider {
    
    private static let urlSession = URLSession.shared
    
    static func request<T: Decodable>(url: URL, queryItems: [URLQueryItem]?, completion: @escaping (Result<T, Error>) -> Void) {
        
        guard var urlComponents = URLComponents(url: url, resolvingAgainstBaseURL: true) else {
            completion(.failure(NSError(domain: "invalid_endpoint", code: 1, userInfo: nil)))
            return
        }
        
        urlComponents.queryItems = queryItems
        
        guard let url = urlComponents.url else {
            completion(.failure(NSError(domain: "invalid_endpoint", code: 1, userInfo: nil)))
            return
        }
        
        if let data = Cache.main.object(forKey: url.absoluteString), let object = try? JSONDecoder().decode(T.self, from: data) {
            completion(.success(object))
            return
        }
        
        urlSession.dataTask(with: url) { (result) in
            switch result {
            case let .success((response, data)):
                guard let statusCode = (response as? HTTPURLResponse)?.statusCode, 200..<299 ~= statusCode else {
                    completion(.failure(NSError(domain: "invalid_response", code: 2, userInfo: ["data" : data])))
                    return
                }
                
                do {
                    let values = try JSONDecoder().decode(T.self, from: data)
                    Cache.main.setObject(data: data, forKey: url.absoluteString)
                    completion(.success(values))
                } catch {
                    completion(.failure(NSError(domain: "decode_error", code: 2, userInfo: ["data" : data])))
                }
                
            case let .failure(error):
                completion(.failure(error))
            }
        }.resume()
    }
}
