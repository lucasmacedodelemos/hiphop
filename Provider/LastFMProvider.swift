//
//  LastFMProvider.swift
//  Provider
//
//  Created by Lucas Macedo de Lemos on 20/07/19.
//  Copyright © 2019 Hip Hop. All rights reserved.
//

import Foundation
import Model

public class LastFMProvider: Provider {
    
    private static var apiKey: String {
        return object(forInfoDictionaryKey: "LAST_FM_API_KEY")
    }
    
    private static var baseURL: URL {
        return URL(string: object(forInfoDictionaryKey: "LAST_FM_BASE_URL"))!
    }
    
    enum Method: String {
        case tagGetTopAlbums = "tag.gettopalbums"
        case albumGetInfo = "album.getinfo"
        case artistGetInfo = "artist.getinfo"
        
        static var key: String {
            return "method"
        }
    }
        
    enum Parameter: String {
        case apiKey = "api_key"
        case tag = "tag"
        case format = "format"
        case page = "page"
        case artist = "artist"
        case album = "album"
        
        var value: String {
            switch self {
            case .format:
                return "json"
            default:
                return ""
            }
        }
    }
    
    public static func fetchTopAlbums(of tag: String, page: String = "1", result: @escaping (Result<TopAlbums, Error>) -> Void) {
        let topAlbumsURL = baseURL
            .appendingPathComponent("2.0")
        let queryItems = [URLQueryItem(name: Method.key, value: Method.tagGetTopAlbums.rawValue),
                          URLQueryItem(name: Parameter.tag.rawValue, value: tag),
                          URLQueryItem(name: Parameter.apiKey.rawValue, value: apiKey),
                          URLQueryItem(name: Parameter.page.rawValue, value: page),
                          URLQueryItem(name: Parameter.format.rawValue, value: Parameter.format.value)]
        
        request(url: topAlbumsURL, queryItems: queryItems, completion: result)
    }
    
    public static func fetchAlbumDetails(of album: String, artist: String, result: @escaping (Result<AlbumDetails, Error>) -> Void) {
        let albumDetailsURL = baseURL
            .appendingPathComponent("2.0")
        let queryItems = [URLQueryItem(name: Method.key, value: Method.albumGetInfo.rawValue),
                          URLQueryItem(name: Parameter.apiKey.rawValue, value: apiKey),
                          URLQueryItem(name: Parameter.artist.rawValue, value: artist),
                          URLQueryItem(name: Parameter.album.rawValue, value: album),
                          URLQueryItem(name: Parameter.format.rawValue, value: Parameter.format.value)]
        
        request(url: albumDetailsURL, queryItems: queryItems, completion: result)
    }
    
    public static func fetchArtistDetails(of artist: String, result: @escaping (Result<ArtistDetails, Error>) -> Void) {
        let artistDetailsURL = baseURL
            .appendingPathComponent("2.0")
        let queryItems = [URLQueryItem(name: Method.key, value: Method.artistGetInfo.rawValue),
                          URLQueryItem(name: Parameter.apiKey.rawValue, value: apiKey),
                          URLQueryItem(name: Parameter.artist.rawValue, value: artist),
                          URLQueryItem(name: Parameter.format.rawValue, value: Parameter.format.value)]
        
        request(url: artistDetailsURL, queryItems: queryItems, completion: result)
    }
}

extension LastFMProvider {
    
    static func object(forInfoDictionaryKey key: String) -> String {
        guard let value = Bundle(for: LastFMProvider.self).object(forInfoDictionaryKey: key) as? String else {
            return ""
        }
        
        return value
    }
}
