//
//  LastFMBusiness.swift
//  Business
//
//  Created by Lucas Macedo de Lemos on 22/07/19.
//  Copyright © 2019 Hip Hop. All rights reserved.
//

import Foundation
import Provider
import Model

public class LastFMBusiness {
    
    public init() {}
    
    public func fetchTopAlbums(of tag: String, page: Int, completion: @escaping (Result<TopAlbums, Error>) -> Void) {
        
        LastFMProvider.fetchTopAlbums(of: tag, page: String(page)) { (result) in
            completion(result)
        }
    }
    
    public func fetchAlbumDetails(of album: String, artist: String, completion: @escaping (Result<AlbumDetails, Error>) -> Void) {
        
        LastFMProvider.fetchAlbumDetails(of: album, artist: artist, result: { (result) in
            completion(result)
        })
    }
    
    public func fetchArtistDetails(of artist: String, completion: @escaping (Result<ArtistDetails, Error>) -> Void) {
        
        LastFMProvider.fetchArtistDetails(of: artist) { (result) in
            completion(result)
        }
    }
    
}
