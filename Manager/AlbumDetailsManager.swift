//
//  AlbumDetailsManager.swift
//  Manager
//
//  Created by Lucas Macedo de Lemos on 26/07/19.
//  Copyright © 2019 Hip Hop. All rights reserved.
//

import Foundation

import Model
import Business


public enum AlbumDetailsType {
    case albumDetails (Result<AlbumDetails, Error>)
    case artistDetails (Result<ArtistDetails, Error>)
}

public protocol AlbumDetailsManagerDelegate: class {
    func handle(type: AlbumDetailsType)
}

public class AlbumDetailsManager {
    
    private weak var delegate: AlbumDetailsManagerDelegate?
    private var business: LastFMBusiness?
    
    public init(delegate: AlbumDetailsManagerDelegate, business: LastFMBusiness? = LastFMBusiness()) {
        self.delegate = delegate
        self.business = business
    }
    
    public func loadAlbumDetails(of album: String, artist: String) {
        business?.fetchAlbumDetails(of: album, artist: artist, completion: { [weak self] (result) in
            self?.delegate?.handle(type: .albumDetails(result))
        })
    }
    
    public func loadArtistDetails(of artist: String) {
        business?.fetchArtistDetails(of: artist, completion: { [weak self] (result) in
            self?.delegate?.handle(type: .artistDetails(result))
        })
    }
}
