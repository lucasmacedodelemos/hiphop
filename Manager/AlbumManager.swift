//
//  HipHopManager.swift
//  Manager
//
//  Created by Lucas Macedo de Lemos on 22/07/19.
//  Copyright © 2019 Hip Hop. All rights reserved.
//

import Foundation
import Model
import Business

public protocol AlbumManagerDelegate: class {
    func handle(result: Result<TopAlbums, Error>)
}

public class AlbumManager {
    
    private weak var delegate: AlbumManagerDelegate?
    private var business: LastFMBusiness?
    
    public init(delegate: AlbumManagerDelegate, business: LastFMBusiness? = LastFMBusiness()) {
        self.delegate = delegate
        self.business = business
    }
    
    public func loadHipHopAlbums(page: Int) {
        business?.fetchTopAlbums(of: "hip-hop", page: page, completion: { [weak self] (result) in
            self?.delegate?.handle(result: result)
        })
    }
}
