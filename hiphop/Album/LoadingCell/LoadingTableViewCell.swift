//
//  LoadingTableViewCell.swift
//  hiphop
//
//  Created by Lucas Macedo de Lemos on 26/07/19.
//  Copyright © 2019 Hip Hop. All rights reserved.
//

import UIKit

class LoadingTableViewCell: UITableViewCell {

    @IBOutlet weak var loading: UIActivityIndicatorView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
