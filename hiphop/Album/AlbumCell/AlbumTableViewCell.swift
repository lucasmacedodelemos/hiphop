//
//  AlbumTableViewCell.swift
//  hiphop
//
//  Created by Lucas Macedo de Lemos on 23/07/19.
//  Copyright © 2019 Hip Hop. All rights reserved.
//

import UIKit
import Model
import Util
import SDWebImage

class AlbumTableViewCell: UITableViewCell {

    @IBOutlet weak var albumImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var artistLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    public func setup(with album: Album) {
        nameLabel.text = album.name
        artistLabel.text = album.artist.name
        albumImageView.sd_setImage(with: URL(string: album.imageText(of: .large))) { (image, error, imageCacheType, url) in
        }
    }
}
