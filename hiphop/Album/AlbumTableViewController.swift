//
//  AlbumTableViewController.swift
//  hiphop
//
//  Created by Lucas Macedo de Lemos on 22/07/19.
//  Copyright © 2019 Hip Hop. All rights reserved.
//

import UIKit
import Manager
import Model
import Hero

class AlbumTableViewController: UITableViewController {

    private lazy var albumManager = AlbumManager(delegate: self)
    private var topAlbums: TopAlbums?
    private var nextPage: Int = 1
    private var fetching = false
    private var shouldShowLoading = true
    private var shouldBackWithLoading = false
    private var selectedCell: AlbumTableViewCell?
    
    enum Section: Int {
        case album = 0
        case loading
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if !fetching {
            fetching = true
            albumManager.loadHipHopAlbums(page: nextPage)
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        selectedCell?.albumImageView.hero.id = nil
        selectedCell?.nameLabel.hero.id = nil
    }
    
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if let albumDetails = segue.destination as? AlbumDetailsViewController {
            selectedCell?.albumImageView.hero.id = "album_image_view"
            selectedCell?.nameLabel.hero.id = "album_name_label"
            
            albumDetails.album = sender as? Album
            
            let backItem = UIBarButtonItem()
            backItem.title = ""
            backItem.tintColor = UIColor(red: 11.0/255, green: 20.0/255, blue: 40.0/255, alpha: 1.0)
            navigationItem.backBarButtonItem = backItem
        }
    }
}

extension AlbumTableViewController {
    // MARK: - Table view data source
    override func numberOfSections(in tableView: UITableView) -> Int {
        var numberOfSections = 1
        
        if let pageString = topAlbums?.albums.attribute.totalPages, let page = Int(pageString)  {
            if self.nextPage <= page {
                numberOfSections += 1
            }
        }
        
        return numberOfSections
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let type = Section(rawValue: section) else {
            return 0
        }
        
        switch type {
        case .album:
            return topAlbums?.albums.album.count ?? 0
        case .loading:
            return shouldShowLoading ? 1 : 0
        }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let type = Section(rawValue: indexPath.section) else {
            return UITableViewCell()
        }
        
        switch type {
        case .album:
            let cell = tableView.dequeueReusableCell(withIdentifier: "albumCell", for: indexPath) as! AlbumTableViewCell
            cell.setup(with: topAlbums!.albums.album[indexPath.row])
            return cell
        case .loading:
            if !fetching {
                fetching = true
                albumManager.loadHipHopAlbums(page: nextPage)
            }
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "loadingCell", for: indexPath) as! LoadingTableViewCell
            cell.selectionStyle = .none
            cell.loading.startAnimating()
            return cell
        }
    }
    
    // MARK: - Table view delegate
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        selectedCell = tableView.cellForRow(at: indexPath) as? AlbumTableViewCell
        
        self.performSegue(withIdentifier: "albumDetailsSegue", sender: topAlbums?.albums.album[indexPath.row])
    }
    
    override func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView.init()
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        guard let type = Section(rawValue: section) else {
            return 0
        }
        
        switch type {
        case .album:
            return 16
        case .loading:
            return 0
        }
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return UIView.init()
    }
}

extension AlbumTableViewController {
    
    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView.contentOffset.y > (scrollView.contentSize.height - tableView.frame.height), !shouldShowLoading, shouldBackWithLoading {
            shouldShowLoading = true
            self.tableView.beginUpdates()
            self.tableView.insertRows(at: [IndexPath(item: 0, section: Section.loading.rawValue)], with: .automatic)
            self.tableView.endUpdates()
        }
    }
    
    override func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        shouldBackWithLoading = true
    }
    
}

extension AlbumTableViewController: AlbumManagerDelegate {
    func handle(result: Result<TopAlbums, Error>) {
        switch result {
        case let .success(topAlbums):
            nextPage += 1
            fetching = false
            
            if self.topAlbums == nil {
                self.topAlbums = topAlbums
            } else {
                self.topAlbums?.albums.attribute = topAlbums.albums.attribute
                self.topAlbums?.albums.album.append(contentsOf: topAlbums.albums.album)
            }
            
            DispatchQueue.main.async { [weak self] in
                self?.tableView.reloadData()
            }
            
        default:
            fetching = false
            shouldShowLoading = false
            shouldBackWithLoading = false
            
            DispatchQueue.main.async { [weak self] in
                self?.tableView.beginUpdates()
                self?.tableView.deleteRows(at: [IndexPath(item: 0, section: Section.loading.rawValue)], with: .automatic)
                self?.tableView.endUpdates()
            }
        }
    }
}
