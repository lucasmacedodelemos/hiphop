//
//  AlbumDetailsViewController.swift
//  hiphop
//
//  Created by Lucas Macedo de Lemos on 25/07/19.
//  Copyright © 2019 Hip Hop. All rights reserved.
//

import UIKit
import Model
import Util
import Manager
import Hero

class AlbumDetailsViewController: UIViewController {

    private lazy var albumDetailsManager = AlbumDetailsManager(delegate: self)

    @IBOutlet weak var stackView: UIStackView!
    @IBOutlet weak var headerImageView: UIImageView!
    @IBOutlet weak var albumImageView: UIImageView!
    @IBOutlet weak var albumLabel: UILabel!
    @IBOutlet weak var tracksCountLabel: UILabel!
    @IBOutlet weak var publishDateLabel: UILabel!
    @IBOutlet weak var artistNameLabel: UILabel!
    @IBOutlet weak var listenersCountLabel: UILabel!
    @IBOutlet weak var headerWidthConstraint: NSLayoutConstraint!
    public var album: Album?
    public var albumDetails: AlbumDetails?
    public var artistDetails: ArtistDetails?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.largeTitleDisplayMode = .never
        
        setupHero()
        
        if let albumName = album?.name, let artistName = album?.artist.name {
            albumDetailsManager.loadAlbumDetails(of: albumName, artist: artistName)
            albumDetailsManager.loadArtistDetails(of: artistName)
        }
        
        headerImageView.sd_setImage(with: URL(string: album?.imageText(of: .large) ?? "")) { (image, error, imageCacheType, url) in
            self.albumImageView.image = image
        }

        addBlur(to: headerImageView)
        
        albumLabel.text = album?.name
    }
    
    private func setupHero() {
        self.hero.isEnabled = true
        albumImageView.hero.id = "album_image_view"
        albumLabel.hero.id = "album_name_label"
    }
    
    private func populateDetails(with albumDetails: AlbumDetails) {
        self.albumDetails = albumDetails
        tracksCountLabel.text = String(albumDetails.album.tracks.track.count)
        publishDateLabel.text = albumDetails.album.wiki.published
    }
    
    private func populateArtist(with artistDetails: ArtistDetails) {
        self.artistDetails = artistDetails
        artistNameLabel.text = artistDetails.artist.name
        listenersCountLabel.text = "\(artistDetails.artist.stats?.listeners ?? "") listeners"
    }
    
    func addBlur(to view: UIView){
        let blurEffect = UIBlurEffect(style: UIBlurEffect.Style.extraLight)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = view.bounds
        blurEffectView.alpha = 0.9
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        view.addSubview(blurEffectView)
    }
    
}

extension AlbumDetailsViewController: UIScrollViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        let yPos: CGFloat = scrollView.contentOffset.y
            
        if yPos < 0 {
            headerWidthConstraint.constant = -yPos
        } else {
            headerWidthConstraint.constant = 0
            
            let maxPosition = (stackView.frame.height - stackView.frame.width)
            if yPos > maxPosition {
                scrollView.contentOffset.y = maxPosition
            }
        }
    }
}

extension AlbumDetailsViewController: AlbumDetailsManagerDelegate {
    func handle(type: AlbumDetailsType) {
        switch type {
        case let .albumDetails(result):
            handleAlbumDetails(result)
        case let .artistDetails(result):
            handleArtistDetails(result)
        }
    }
    
    private func handleAlbumDetails(_ result: Result<AlbumDetails, Error>) {
        switch result {
        case let .success(albumDetais):
            DispatchQueue.main.async {
                self.populateDetails(with: albumDetais)
            }
        case .failure(_):
            break
        }
    }
    
    private func handleArtistDetails(_ result: Result<ArtistDetails, Error>) {
        switch result {
        case let .success(artistDetails):
            DispatchQueue.main.async {
                self.populateArtist(with: artistDetails)
            }
        case .failure(_):
            break
        }
    }
}
