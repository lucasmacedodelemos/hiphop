//
//  URLSession.swift
//  Util
//
//  Created by Lucas Macedo de Lemos on 19/07/19.
//  Copyright © 2019 Hip Hop. All rights reserved.
//

import Foundation

extension URLSession {

    public func dataTask(with url: URL, result: @escaping (Result<(URLResponse, Data), Error>) -> Void) -> URLSessionDataTask {
        return dataTask(with: url, completionHandler: { (data, response, error) in
            if let _ = error {
                let code = (response as? HTTPURLResponse)?.statusCode ?? 0
                
                var userInfo: [String : Any] = [:]
                if let data = data {
                    userInfo["data"] = data
                }
                
                result(.failure(NSError(domain: "error", code: code, userInfo: userInfo)))
                return
            }
            
            guard let data = data else {
                let code = (response as? HTTPURLResponse)?.statusCode ?? 0
                result(.failure(NSError(domain: "error", code: code, userInfo: nil)))
                return
            }
            
            guard let response = response else {
                result(.failure(NSError(domain: "error", code: 0, userInfo: ["data" : data])))
                return
            }
            
            result(.success((response, data)))
        })
    }
}
