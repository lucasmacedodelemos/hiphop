//
//  Cache.swift
//  Util
//
//  Created by Lucas Macedo de Lemos on 24/07/19.
//  Copyright © 2019 Hip Hop. All rights reserved.
//

import Foundation

struct CachedObject: Codable {
    var data: Data!
    var date: Date!
}

public class Cache {
    
    public static let main = Cache()
    
    private var cacheTime: TimeInterval = 120
    
    private init() {}
    
    public func configure(cacheTime: TimeInterval) {
        Cache.main.cacheTime = cacheTime
    }
    
    public func setObject(data: Data, forKey key: String) {
        let cachedObject = CachedObject(data: data, date: Date())
        let cachedData = try? JSONEncoder().encode(cachedObject)
        
        let userDafault = UserDefaults.init(suiteName: UserDefaults.argumentDomain)
        userDafault?.set(cachedData, forKey: key)
    }
    
    public func object(forKey key: String) -> Data? {
        let userDafault = UserDefaults.init(suiteName: UserDefaults.argumentDomain)
        guard let data = userDafault?.object(forKey: key) as? Data else {
            return nil
        }
        
        guard let cachedObject = try? JSONDecoder().decode(CachedObject.self, from: data) else {
            return nil
        }
        
        if !isConnected() {
            return cachedObject.data
        }
        
        let date = Date()
        let variation = date.timeIntervalSinceNow - cachedObject.date.timeIntervalSinceNow
        
        if variation > Cache.main.cacheTime {
            return nil
        }
        
        return cachedObject.data
    }
    
    private func isConnected() -> Bool {
        if let reachability = Reachability(), reachability.connection == .none {
            return false
        }
        
        return true
    }
}
