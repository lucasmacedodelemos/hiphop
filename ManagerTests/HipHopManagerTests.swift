//
//  HipHopManagerTests.swift
//  ManagerTests
//
//  Created by Lucas Macedo de Lemos on 22/07/19.
//  Copyright © 2019 Hip Hop. All rights reserved.
//

import XCTest
import Model
@testable import Manager

class HipHopManagerTests: XCTestCase {

    private lazy var manager = AlbumManager(delegate: self)
    private var topAlbums: TopAlbums?
    var expectation: XCTestExpectation?
    
    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testHipHopTag() {
        expectation = self.expectation(description: "hip_hop_top_albums")

        manager.loadHipHopAlbums(page: 1)
        waitForExpectations(timeout: 8, handler: nil)
        XCTAssertTrue(topAlbums?.albums.attribute.tag == "hip hop")
    }

}

extension HipHopManagerTests: AlbumManagerDelegate {
    func handle(result: Result<TopAlbums, Error>) {
        switch result {
        case let .success(topAlbums):
            self.topAlbums = topAlbums
        default:
            break
        }
        
        expectation?.fulfill()
    }
}
